const headerHeight = 65;
const footerHeight = 100;

initApplication();

function initApplication() {
    window.addEventListener('load', onApplicationReady);
    window.addEventListener('resize', resizeEventHandler);
}

function resizeEventHandler() {
    const documentHeight = document.documentElement.clientHeight;
    const documentContentWrapper = document.querySelector('#wrapper');
    const calculatedContentHeight = documentHeight - (headerHeight + footerHeight);

    documentContentWrapper.style.minHeight = calculatedContentHeight + 'px';
}

function onApplicationReady() {
    resizeEventHandler();
    const MenuItemList = document.querySelectorAll(".menu-item");
    MenuItemList.forEach(menu => initMenu(menu));
}

function initMenu(menu) {
    menu.addEventListener('mouseover', function () {
        this.querySelector('.sub-menu').classList.add('item-visible');
    });

    menu.addEventListener('mouseout', function () {
        this.querySelector('.sub-menu').classList.remove('item-visible');
    });

    const subMenuLinkList = menu.querySelector('.sub-menu').querySelectorAll('.sub-link');
    subMenuLinkList.forEach((submenu) => {
        const targetLink = submenu.getAttribute('data-href');
        submenu.addEventListener('click', function () {
            window.location.href = targetLink;
        });
    });
}