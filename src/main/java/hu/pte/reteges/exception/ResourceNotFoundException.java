package hu.pte.reteges.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ResourceNotFoundException extends Exception {

    private static final long serialVersionUID = 20163635769692836L;

    private final Class clazz;
    private final Long id;

    @Override
    public String getMessage() {
        return "Nincs ilyen rekord az adatbázisban. Típus: " + this.clazz.getName() + ", ID: " + this.id;
    }
}
