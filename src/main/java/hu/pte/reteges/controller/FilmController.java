package hu.pte.reteges.controller;

import hu.pte.reteges.entity.Film;
import hu.pte.reteges.entity.Studio;
import hu.pte.reteges.exception.ResourceNotFoundException;
import hu.pte.reteges.service.FilmService;
import hu.pte.reteges.service.StudioService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class FilmController {

    private final FilmService filmService;
    private final StudioService studioService;

    @GetMapping("/film")
    public String index(Model model) {
        model.addAttribute("films", this.filmService.findAllFilms());
        return "templates/film";
    }

    @GetMapping("/film/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        try {
            model.addAttribute("film", this.filmService.findOneById(id));
            model.addAttribute("studios", this.studioService.findAllStudios());
        } catch (ResourceNotFoundException e) {
            e.printStackTrace();
        }

        return "templates/edit/film";
    }

    @PostMapping(value = "/film/{id}")
    public String update(@PathVariable("id") Long id, Model model, Film film) {
        try {
            Studio studio = this.studioService.findOneById(film.getStudio().getId());
            // film.setStudio(studio);
        } catch (ResourceNotFoundException e) {
            e.printStackTrace();
        }
        this.filmService.persistFilm(film);

        return "templates/film";
    }

    @GetMapping(value = "/books/{id}/delete")
    public String deleteById(@PathVariable("id") Long id) {
        this.filmService.deleteFilmById(id);

        return "redirect:/film";
    }
}
