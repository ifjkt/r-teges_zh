package hu.pte.reteges.service;

import hu.pte.reteges.entity.Film;
import hu.pte.reteges.exception.ResourceNotFoundException;

import java.util.List;

public interface FilmService {

    public Film findOneById(Long id) throws ResourceNotFoundException;

    public List<Film> findAllFilms();

    public void persistAll(List<Film> films);

    public void persistFilm(Film film);

    public void deleteFilm(Film film);

    public void deleteFilmById(Long id);
}
