package hu.pte.reteges.service;

import hu.pte.reteges.entity.Studio;
import hu.pte.reteges.exception.ResourceNotFoundException;

import java.util.List;

public interface StudioService {

    public Studio findOneById(Long id) throws ResourceNotFoundException;

    public List<Studio> findAllStudios();

    public void persistStudio(Studio studio);

    public void deleteStudio(Studio studio);

    public void deleteStudioById(Long id);
}
