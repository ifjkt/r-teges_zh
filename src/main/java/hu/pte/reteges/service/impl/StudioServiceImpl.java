package hu.pte.reteges.service.impl;

import hu.pte.reteges.entity.Studio;
import hu.pte.reteges.exception.ResourceNotFoundException;
import hu.pte.reteges.repository.StudioRepository;
import hu.pte.reteges.service.StudioService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class StudioServiceImpl implements StudioService {

    private final StudioRepository studioRepository;

    @Override
    public Studio findOneById(Long id) throws ResourceNotFoundException {
        return this.studioRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Studio.class, id));
    }

    @Override
    public List<Studio> findAllStudios() {
        return this.studioRepository.findAll();
    }

    @Override
    public void persistStudio(Studio studio) {
        this.studioRepository.save(studio);
    }

    @Override
    public void deleteStudio(Studio studio) {
        this.studioRepository.delete(studio);
    }

    @Override
    public void deleteStudioById(Long id) {
        this.studioRepository.deleteById(id);
    }
}
