package hu.pte.reteges.service.impl;

import hu.pte.reteges.entity.Film;
import hu.pte.reteges.exception.ResourceNotFoundException;
import hu.pte.reteges.repository.FilmRepository;
import hu.pte.reteges.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class FilmServiceImpl implements FilmService {

    private final FilmRepository filmRepository;

    @Override
    public Film findOneById(Long id) throws ResourceNotFoundException {
        return this.filmRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Film.class, id));
    }

    @Override
    public List<Film> findAllFilms() {
        return this.filmRepository.findAll();
    }

    @Override
    public void persistAll(List<Film> films) {
        this.filmRepository.saveAll(films);
    }

    @Override
    public void persistFilm(Film film) {
        this.filmRepository.save(film);
    }

    @Override
    public void deleteFilm(Film film) {
        this.filmRepository.delete(film);
    }

    @Override
    public void deleteFilmById(Long id) {
        this.filmRepository.deleteById(id);
    }
}
