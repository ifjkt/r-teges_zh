package hu.pte.reteges;

import com.thedeanda.lorem.LoremIpsum;
import hu.pte.reteges.entity.Film;
import hu.pte.reteges.entity.Studio;
import hu.pte.reteges.service.FilmService;
import hu.pte.reteges.service.StudioService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Transactional
@RequiredArgsConstructor
public class Bootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private static final int GENERATED_FILM_COUNT = 10;
    private static final int GENERATED_FILM_NAME_LENGTH = 2;
    private static final int GENERATED_STUDIO_COUNT = 10;

    private final FilmService filmService;
    private final StudioService studioService;
    private final LoremIpsum generator = LoremIpsum.getInstance();

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<Film> filmList = new ArrayList<>();

        for (int i = 0; i < GENERATED_FILM_COUNT; i++) {
            Film film = new Film();
            film.setName(this.generator.getTitle(GENERATED_FILM_NAME_LENGTH));
            film.setReleaseDate(new Date());

            Studio studio = new Studio();
            studio.setName(this.generator.getWords(1));
            studio.setDateOfEstablishment(new Date());
            film.setStudio(studio);

            filmList.add(film);
        }

        this.filmService.persistAll(filmList);
    }
}
