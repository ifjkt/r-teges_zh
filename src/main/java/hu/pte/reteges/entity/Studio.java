package hu.pte.reteges.entity;

import hu.pte.reteges.entity.audit.AuditedEntityBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Audited
@AuditTable(value = "h_studios")
@EqualsAndHashCode(callSuper = true)
@Table(name = "studios", schema = "public")
@SequenceGenerator(name = "id_generator", sequenceName = "seq_studios", allocationSize = 1)
public class Studio extends AuditedEntityBase {

    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfEstablishment;

    @OneToOne(mappedBy = "studio")
    private Film film;
}
