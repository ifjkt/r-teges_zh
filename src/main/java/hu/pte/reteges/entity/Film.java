package hu.pte.reteges.entity;

import hu.pte.reteges.entity.audit.AuditedEntityBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Audited
@AuditTable(value = "h_films")
@EqualsAndHashCode(callSuper = true)
@Table(name = "films", schema = "public")
@SequenceGenerator(name = "id_generator", sequenceName = "seq_film", allocationSize = 1)
public class Film extends AuditedEntityBase {

    private String name;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date releaseDate;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "studio_id", referencedColumnName = "id")
    private Studio studio;
}
