package hu.pte.reteges.entity.audit;

import hu.pte.reteges.enumerated.DmlFlag;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
@Where(clause = "dml_flag <> D")
public class AuditedEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
    private Long id;

    @CreationTimestamp
    @Column(name = "creation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;

    @UpdateTimestamp
    @Column(name = "update_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTime;

    @Setter(AccessLevel.NONE)
    @Column(name = "dml_flag")
    @Enumerated(EnumType.STRING)
    private DmlFlag dmlFlag;

    @PreRemove
    private void deleteAction() {
        this.dmlFlag = DmlFlag.D;
    }

    @PrePersist
    private void insertAction() {
        this.dmlFlag = DmlFlag.I;
    }

    @PreUpdate
    private void updateAction() {
        this.dmlFlag = DmlFlag.U;
    }
}

