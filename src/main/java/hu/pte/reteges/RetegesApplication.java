package hu.pte.reteges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class RetegesApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetegesApplication.class, args);
    }
}